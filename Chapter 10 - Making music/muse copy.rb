require './wav'

class Song
  attr :wav_file, :bars

  def self.record(name, &block)
    puts "Start recording song named #{name}.wav"
    @wav_file = Wav.new(name + ".wav")
    @bars = {}
    puts "Processing ..."
    instance_eval &block
    save
    puts "done."
  end

  class Bar
    attr :stream, :bpm, :beats, :time

    FREQUENCIES = {
      :a2   => -24, :ais2 => -23, :b2   => -22,  :c2   => -21, :cis2 => -20, 
      :d2   => -19, :dis2 => -18, :e2   => -17,  :f2   => -16, :fis2 => -15, 
      :g2   => -14, :gis2 => -13, :a3   => -12,  :ais3 => -11, :b3   => -10, 
      :c3   => -9,  :cis3 => -8,  :d3   => -7,   :dis3 => -6,  :e3   => -5,  
      :f3   => -4,  :fis3 => -3,  :g3   => -2,   :gis3 => -1,  :a4   => 0,
      :ais4 => 1,   :b4   => 2,   :c4   => 3,    :cis4 => 4,   :d4   => 5, 
      :dis4 => 6,   :e4   => 7,   :f4   => 8,    :fis4 => 9,   :g4   => 10,   
      :gis4 => 11,  :a5   => 12,  :ais5 => 13,   :b5   => 14,  :c5   => 15,
      :cis5 => 16,  :d5   => 17,  :dis5 => 18,   :e5   => 19,  :f5   => 20,
      :fis5 => 21,  :g5   => 22,  :gis5 => 23
    }

    def initialize(id, options={})
      @bpm = options[:bpm] || 120
      @beats = options[:beats]
      @time = options[:time]
      @stream = []
    end

    def notes(&block)
      instance_eval &block
    end    

    def frequency_of(step)
      440.0*(2**(step.to_f/12.0))
    end

    def chord(notes,options={})
      chord =[]
      notes.each do |name|
        if %w(_ a ais b c cis d dis e f fis g gis).include? name.to_s
          note = name.to_s
          chord << note_data(note, options)
        end
      end
      @stream += chord.transpose.map {|x| x.transpose.map {|y| y.reduce(:+)}}   
    end

    def note_data(note, options={})
      stream = []
      if options
        beats  = options[:b].nil?  ? (@beats || 1) : options[:b].to_f
        volume = (options[:v].nil? ? 10 : options[:v].to_i) * 1000
        octave = options[:o].to_i + 3
      else
        beats, volume, octave = (@beats || 1), 10000, 3
      end
      puts "beats : #{beats}"
      duration = ((60 * Wav::SAMPLE_RATE * beats)/@bpm)/Wav::SAMPLE_RATE.to_f
      note_frequency = note + octave.to_s
      unless note == '_'
        freq = frequency_of(FREQUENCIES[note_frequency.to_sym])
      else
        freq = 0
      end      
      (0.0..duration.to_f).step(1.0/Wav::SAMPLE_RATE) do |i|
        x = (volume * Math.sin(2 * Math::PI * freq * i)).to_i
        stream << [x,x]
      end 
      puts "stream size : #{stream.length}"      
      return stream           
    end

    def method_missing(name, *args, &block)
      if %w(_ a ais b c cis d dis e f fis g gis).include? name.to_s
        note = name.to_s
        @stream += note_data(note, args[0])
      end
    end
  end

  private
  class << self
    def bar(id, options={})
      puts "bar #{id}"
      unless @bars[id]
        @bars[id] = []
      end
      @bars[id] << Bar.new(id, options)
      @bars[id].last
    end

    def save
      puts "Saving music file"
      stream = []
      @bars.each do |id, item|
        container = []
        item.each do |i|
          container << i.stream
        end
        stream += container.transpose.map {|x| x.transpose.map {|y| y.reduce(:+)}}   
      end
      @wav_file.write(stream)
      @wav_file.close
    end
  end
end